<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package applied-handling
 */

?><!doctype html>

<html <?php language_attributes(); ?>>
<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <title>Applied Handling</title>

    <link href='https://fonts.googleapis.com/css?family=Karla' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <style>
        .breadcrumbs a:active, .breadcrumbs a:visited {
        	color: #ffffff;
        	text-decoration: none;
        }
    </style>

    <?php wp_head(); ?>
    <!-- Q&A Updates by Element5 on 5/5/16 -->
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/assets/css/update.css" />
    <script type="text/javascript">
        jQuery(function() {
            $('.menu-item').hover(
                
               function () {
                  $('.sub-menu-container').css({"opacity":"1"});
               }, 
                
               function () {
                  $('.sub-menu-container').css({"opacity":"0"});
               }
            );
        });

    </script>

    <!-- CALL TRACKING 5/30 -->
    <script type="text/javascript">
    (function(a,e,c,f,g,h,b,d){var k={ak:"851899072",cl:"NUdbCP27uHEQwOWblgM"};a[c]=a[c]||function(){(a[c].q=a[c].q||[]).push(arguments)};a[g]||(a[g]=k.ak);b=e.createElement(h);b.async=1;b.src="//www.gstatic.com/wcm/loader.js";d=e.getElementsByTagName(h)[0];d.parentNode.insertBefore(b,d);a[f]=function(b,d,e){a[c](2,b,k,d,null,new Date,e)};a[f]()})(window,document,"_googWcmImpl","_googWcmGet","_googWcmAk","script");
    </script>
</head>

<body <?php body_class(); ?> onload="_googWcmGet('number', '1-800-837-8020')">
    <div class="container">

        <header>
            <div class="wrapper">

                <a class="logo" href="/">
                    <img src="/wp-content/themes/applied-handling/assets/images/logo.png" alt="" title="" />
                </a>

                <div class="menus">

                    <nav class="main">
                        <?php
                            echo wp_nav_menu([
                                'menu' => 'primary-menu',
                                'container' => '',
                                'menu_class' => '',
                                'link_after' => '<span class="arrow"></span>',
                                'depth' => 1
                            ]);
                        ?>
                    </nav>

                    <nav class="top">
                        <div class="wrap">
                            <?php wp_nav_menu( array( 
                                'theme_location' => 'top-nav', 
                                'container' => 'li', 
                                'container_class' => false
                            ) ); ?>
                        </div>
                        <div class="mobile-search search">
                            <?php get_search_form(); ?>
                        </div>
                    </nav>

                    <div class="social-icons">
                        <ul>
                            <li class="facebook"><a href="#">Facebook</a></li>
                            <li class="twitter"><a href="#">twitter</a></li>
                            <li class="youtube"><a href="#">YouTube</a></li>
                        </ul>
                    </div>

                </div>

                <div class="search">
                    <?php get_search_form(); ?>
                </div>

                <a class="button phone" href="tel:18008378020" onClick="ga('send', 'event', { eventCategory: 'Phone Number', eventAction: 'Click', eventLabel: '<?php the_title() ?> | 1-800-837-8020'});">
                    <span class="icon"></span>
                    <span class="call">Call</span>
                    <span class="number">+1 (800) 837-8020</span>
                </a>

                <a class="button menu mobile-only"><span class="icon"></span><span class="title"><?php esc_html_e( 'Menu', 'applied-handling' ); ?></span></a>

            </div>

            <div class="sub-menu-container">
                <div class="wrapper">

                <?php

                    $menu_items = wp_get_nav_menu_items( 'primary-menu' );

                    $parents = 0;
                    $menu_list = '';
                    foreach ( (array) $menu_items as $key => $menu_item ) {

                        // skip items with a parent -- output opening list
                        if( $menu_item->post_parent == 0 ){

                            if( $parents > 0 ){

                                $menu_list .= '</ul>';
                            }
                            $menu_list .= '<ul id="menu-' . $menu_item->ID . '" class="sub-menu">';
                            $parents++;

                            continue;
                        }

                        $title = $menu_item->title;
                        $url = $menu_item->url;

                        $menu_list .= '<li class="menu-item">';

                        $menu_list .= '<a href="' . $url . '">';

                        // Get post featured image url
                        $page_id = get_post_meta( $menu_item->ID, '_menu_item_object_id', true );
                        $feature_image_meta = wp_get_attachment_image_src( get_post_thumbnail_id( $page_id ), 'thumbnail-size' );
                        $feat_image = $feature_image_meta[0];

                        if ( !empty( $feat_image ) ){

                            $menu_list .= '<img class="thumb" src="'.$feat_image.'" />';
                        }
                        else{

                            $menu_list .= '<img class="thumb" src="'.get_stylesheet_directory_uri().'/assets/images/products/dock-leveler.png" />';
                        }

                        $menu_list .= '<span>' . $title . '</span><div class="menu-overlay"><div class="view">View</div></div></a>';
                        $menu_list .= '</li>';
                    }
                    $menu_list .= '</ul>';
                    echo $menu_list;


                ?>
                <script type="text/javascript">

                    jQuery( document ).ready(function($){

                        // only show menu on desktop and up
                        if( $( window ).width() < 768 ) return;

                        $("#menu-primary-menu").find(".menu-item").on( 'mouseenter', function(){

                            $("#menu-primary-menu").find(".hover").removeClass("hover");

                            var hover_menu_id = $(this).attr("id").replace( "menu-item-", "");

                            // hide other potential visible menus
                            $(".sub-menu-container").hide();
                            $(".sub-menu-container").find('.sub-menu').removeClass( 'show' );

                            // show sub menu if it exists
                            if( $("#menu-" + hover_menu_id ).find("li").length > 0 ){

                                $(this).addClass("hover");
                                $(".sub-menu-container").show();
                                $("#menu-" + hover_menu_id ).addClass( 'show' );
                            }
                            else{

                                $(this).addClass("hover");
                            }
                            
                        });

                        $("#menu-primary-menu").find(".menu-item").on( 'mouseleave', function( event ){

                            var toElem = $(event.relatedTarget) || $(event.toElement);

                            if ( toElem.hasClass("sub-menu-container") || toElem.hasClass("sub-menu") || toElem.hasClass("main") ) return;
                            if ( toElem.attr("id") == "menu-primary-menu" ) return;

                            $("#menu-primary-menu").find(".hover").removeClass("hover");
                            $(".sub-menu-container").hide();
                            $(".sub-menu-container").find('.sub-menu').removeClass( 'show' );

                        });

                        $(".sub-menu-container").on( 'mouseleave', function( event ){

                            var toElem = $(event.relatedTarget) || $(event.toElement);

                            if ( toElem.hasClass("sub-menu-container") || toElem.hasClass("sub-menu") || toElem.hasClass("main") ) return;
                            if ( toElem.attr("id") == "menu-primary-menu" ) return;

                            $("#menu-primary-menu").find(".hover").removeClass("hover");
                            $(".sub-menu-container").hide();
                            $(".sub-menu-container").find('.sub-menu').removeClass( 'show' );

                        });

                    });

                </script>

<!--                    <ul id="menu-1" class="sub-menu">-->
<!--                        <li class="menu-item">-->
<!--                            <a href="#">-->
<!--                                <img class="thumb" src="/assets/images/products/dock-leveler.png" alt="/assets/images/products/dock-leveler.png" />-->
<!--                                <span>Levelers</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li class="menu-item">-->
<!--                            <a href="#">-->
<!--                                <img class="thumb" src="/assets/images/products/dock-leveler.png" alt="/assets/images/products/dock-leveler.png" />-->
<!--                            <span>Restraints-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li class="menu-item">-->
<!--                            <a href="#">-->
<!--                                <img class="thumb" src="/assets/images/products/dock-leveler.png" alt="/assets/images/products/dock-leveler.png" />-->
<!--                                <span>Seals &amp; Shelters</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li class="menu-item">-->
<!--                            <a href="#">-->
<!--                                <img class="thumb" src="/assets/images/products/dock-leveler.png" alt="/assets/images/products/dock-leveler.png" />-->
<!--                                <span>Truck Levelers</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li class="menu-item">-->
<!--                            <a href="#">-->
<!--                                <img class="thumb" src="/assets/images/products/dock-leveler.png" alt="/assets/images/products/dock-leveler.png" />-->
<!--                                <span>Specialty &amp; Misc.</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li class="menu-item">-->
<!--                            <a href="#">-->
<!--                                <img class="thumb" src="/assets/images/products/dock-leveler.png" alt="/assets/images/products/dock-leveler.png" />-->
<!--                                <span>Control Panels</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                    </ul>-->

                </div>
            </div>

        </header>