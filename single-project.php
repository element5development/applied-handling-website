<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package applied-handling
 */

get_header(); ?>

    <div class="slides blog-article">
        <ul>
            <li class="slide" style="background-image: url(<?php the_field( 'post_header_image' ) ?>);">
                <div class="wrapper">
                    <h1 class="title"><?php echo get_the_title(); ?></h1>
                </div>
                <div class="header-overlay"></div>
            </li>
        </ul>
    </div>


    <article class="project-article">
            <?php the_content(); ?>
    </article>

    <div class="key-services">
        <div class="wrapper">
            <h3 class="title">Key Services</h3>
            <div class="services-container">
                <?php 
                    $selected = get_field('key_services');
                    if( in_array('energy', $selected) ) { 
                        ?>
                            <div class="key-service-container">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/icon-energy-savings.svg" />
                                <p>Energy Savings</p> 
                            </div>
                        <?php
                    }
                    if( in_array('engineered', $selected) ) { 
                        ?>
                            <div class="key-service-container">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/icon-engineered-solutions.svg" />
                                <p>Engineered Solutions</p> 
                            </div>
                        <?php
                    }
                    if( in_array('construction', $selected) ) { 
                        ?>
                            <div class="key-service-container">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/icon-construction.svg" />
                                <p>Construction</p> 
                            </div>
                        <?php
                    }
                    if( in_array('product', $selected) ) { 
                        ?>
                            <div class="key-service-container">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/icon-product-application.svg" />
                                <p>Product Application</p> 
                            </div>
                        <?php
                    }
                ?>
            </div>
            <div style="clear: both"></div>
        </div>
    </div>


    <div class="share-article">

        <div class="wrapper">

            <h3 class="title">Share This Article</h3>

            <div class="social-icons social-share">
                <ul>
                    <li class="facebook"><a target="_blank" href="http://www.facebook.com/sharer/sharer.php?s=100&amp;p[url]=<?php the_permalink(); ?>">Facebook</a></li>
                    <li class="twitter"><a target="_blank" href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>;text=<?php the_title(); ?>&amp;via=ahinow">twitter</a></li>
                    <li class="linkedin"><a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>">LinkedIn</a></li>
                    <li class="email"><a href="mailto:?&subject=<?php the_title(); ?>&body=<?php the_permalink(); ?>">Email</a></li>
                </ul>
            </div>
        </div>
    </div>


<?php
get_sidebar();
get_footer();
