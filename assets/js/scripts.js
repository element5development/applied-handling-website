jQuery(document).ready( function($){

    /** Mobile Menu **/
    $( ".menu" ).click(function(){

        $(this).toggleClass( "active" );
        if( $(this).hasClass("active") ){

            $(this).find(".title").text("Close");
            $(".menus").fadeIn();
        }
        else {

            $(this).find(".title").text("Menu");
            $(".menus").fadeOut();
        }
    });


    /** Mobile Locations **/
    $(".locations").find(".title").click(function(){

        var icon = $(this).find(".icon");

        if( $(icon).is(":visible") ) {

            $(this).siblings(".locations-list").toggleClass("active");
        }

    });

    /** Contact Service Area **/
    $(".contact-locations").find(".location").find(".service").click(function( evt ){

        evt.preventDefault();

        if( $(this).hasClass("active") ){

            $(this).removeClass("active");
            $(this).siblings(".service-area").removeClass("active");
        }
        else{

            $(this).addClass("active");
            $(this).siblings(".service-area").addClass("active");
        }
    });
});