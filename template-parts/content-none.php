<?php
/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package applied-handling
 */

?>

<section class="no-results not-found">
	<div class="slides">
        <ul>
            <li class="slide about-us">
                <div class="wrapper">
                    <h1 class="title">Nothing Found</h1>
                </div>
            </li>
        </ul>
    </div>

	<div class="page-content">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( wp_kses( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'applied-handling' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>
			<h2 style="text-align: center;">Sorry, we had no luck finding any results for your search.</h2>
			<div class="post-feed">
				<aside id="" class="widget-area-container">
					<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('blog-sidebar')) : else : ?>
						<p><strong>Widget Ready</strong></p>  
					<?php endif; ?>  
				</aside>
			</div>

			<?php
		else : ?>

			<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'applied-handling' ); ?></p>
			<?php
				get_search_form();

		endif; ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
