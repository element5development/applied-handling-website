<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package applied-handling
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="post-feed-image"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a></div>
    <div class="content">
        <div class="meta"><span class="date"><?php the_date(); ?></span> <!--|  <span class="tags"><a href="#" class="tag">Tips</a></span>--></div>
        <h3 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <p class="excerpt"><?php echo get_excerpt(); ?></p>
        <a class="read-more" href="<?php the_permalink(); ?>">read more</a>
        <div class="social">
            <ul>
                <li class="facebook"><a target="_blank" href="http://www.facebook.com/sharer/sharer.php?s=100&amp;p[url]=<?php the_permalink(); ?>">Facebook</a></li>
                <li class="twitter"><a target="_blank" href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>;text=<?php the_title(); ?>&amp;via=ahinow">twitter</a></li>
                <li class="linkedin"><a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>">LinkedIn</a></li>
            </ul>
        </div>
    </div>
</article><!-- #post-## -->