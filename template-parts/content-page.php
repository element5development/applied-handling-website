<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package applied-handling
 */

?>

<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?> 

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="slides">
            <ul>
                <li class="slide" style="background-image: url(<?php echo $src[0]; ?>);">
                    <div class="wrapper">
                        <?php the_title( '<h2 class="title">', '</h2>' ); ?>
                        <div class="breadcrumbs">
                            <a href="/">Home</a>
                            <span class="separator">&gt;</span>
                            <span class="current"><?php the_title(); ?></span>
                        </div>
                    </div>
                    <div class="header-overlay"></div>
                </li>
            </ul>
        </div>

        <div class="content project-content">
            <div class="wrapper">

		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'applied-handling' ),
				'after'  => '</div>',
			) );
		?>

	<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'applied-handling' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->

            </div>
        </div>
</article><!-- #post-## -->
