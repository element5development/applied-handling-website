<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package applied-handling
 */

?>
        <footer>

        <?php if ( is_front_page() ) { ?>
            <div class="red-band stay-informed">
                <div class="wrapper">
                    <h3 class="title">Stay Informed!</h3>
                    <p>Sign up and let the news come to you. We promise to keep it interesting</p>
                    <?php echo do_shortcode('[gravityform id="6" title="false" description="false"]'); ?>
                </div>
            </div>
        <?php } ?>

        <?php if ( is_page(2) ) { ?>
            <div class="red-band stay-informed">
                <div class="wrapper">
                    <h3 class="title">Interested in working with us?</h3>
                    <p class="about-cta-content">Check out our career page for open positions!</p>
                    <a href="/about/employment/" class="cta-button">View Employment Opportunities</a>
                </div>
            </div>
        <?php } ?>

            <div class="middle">
                <div class="wrapper">

                    <nav class="footer-nav">
                        <ul class="col">
                            <li><a href="/">Home</a></li>
                            <li><a href="/about">About</a></li>
                            <li><a href="/about/#history">History</a></li>
                            <li><a href="/projects">Projects</a></li>
                            <li><a href="/blog">Blog</a></li>
                            <li><a href="/about/employment/">Careers</a></li>
                            <li><a target="_blank" href="http://connect.appliedhandling.com/">Associates</a></li>
                        </ul>
                        <ul class="col">
                            <li><a href="/loading-docks">Loading Docks</a></li>
                            <li><a href="/industrial-doors">Doors</a></li>
                            <li><a href="/industrial-fans">Fans</a></li>
                            <li><a href="/lifts-storage">Lifts &amp; Storage</a></li>
                            <li><a href="/barriers-gates">Gates &amp; Walls</a></li>
                            <li><a href="/service-construction">Service</a></li>
                        </ul>
                    </nav>

                    <div class="locations">
                        <h3 class="title">Locations <span class="icon"></span></h3>
                        <div class="locations-list">

                            <div class="location">
                                <p class="name">Dearborn</p>
                                <p class="address">15200 Century Dr<br />
                                                    Dearborn, MI 48120</p>
                                <p class="phone"><a href="tel:3133368020" onClick="ga('send', 'event', { eventCategory: 'Phone Number', eventAction: 'Click', eventLabel: '<?php the_title() ?> | 313-336-8020'});"><span class="number">(313) 336-8020</span></a>
                                <!--<p class="directions"><a href="#">Directions</a></p>-->
                            </div>
                            <div class="location">
                                <p class="name">Flint</p>
                                <p class="address">221 S. Mill St. Ste. 2<br />
                                    Clio, MI 48420</p>
                                <p class="phone"><a href="tel:8102759490" onClick="ga('send', 'event', { eventCategory: 'Phone Number', eventAction: 'Click', eventLabel: '<?php the_title() ?> | 810-275-9490'});"><span class="number">(810) 275-9490</span></a>
                                <!--<p class="directions"><a href="#">Directions</a></p>-->
                            </div>
                            <div class="location">
                                <p class="name">Grand Rapids</p>
                                <p class="address">7425 Clyde Park SW Ste E<br />
                                    Byron Center, MI 49315</p>
                                <p class="phone"><a href="tel:6168785370" onClick="ga('send', 'event', { eventCategory: 'Phone Number', eventAction: 'Click', eventLabel: '<?php the_title() ?> | 616-878-5370'});"><span class="number">(616) 878-5370</span></a></p>
                                <!--<p class="directions"><a href="#">Directions</a></p>-->
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div class="contact">
                <div class="wrapper">

                    <div class="phone">
                        <div class="wrapper">

                            <span class="icon"></span>
                            <p>Call us Today! We'd love to hear from you!</p>
                            <a href="tel:8008378020" onClick="ga('send', 'event', { eventCategory: 'Phone Number', eventAction: 'Click', eventLabel: '<?php the_title() ?> | 1-800-837-8020'});"><span class="number">+1 (800) 837-8020</span></a>

                        </div>
                    </div>

                    <div class="social-icons">
                        <ul>
                            <li class="facebook"><a target="_blank" href="https://www.facebook.com/ahinow/">Facebook</a></li>
                            <li class="twitter"><a target="_blank" href="https://twitter.com/ahinow">twitter</a></li>
                            <li class="youtube"><a target="_blank" href="https://www.youtube.com/user/ahinow">YouTube</a></li>
                        </ul>
                    </div>

                </div>
            </div>

            <div class="bottom-bar">
                <div class="wrapper">

                    <p class="copy">&copy; Copyright <?php echo date('Y'); ?> Applied Handling. All Rights Reserved. Web Design by <a target="_blank" href="http://element5digital.com/">Element5</a></p>

                </div>
            </div>

        </footer>

    </div>

    <script src="/wp-content/themes/applied-handling/assets/js/all.js"></script>

<?php wp_footer(); ?>

</body>

</html>