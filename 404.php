<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package applied-handling
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
		        <div class="slides">
				    <ul>
				        <li class="slide about-us">
				            <div class="wrapper">
				                <h1 class="title">Oops! That page can&rsquo;t be found.</h1>
				            </div>
				        </li>
				    </ul>
				</div>

				<div class="page-content error">
					<aside id="error-sidebar" class="widget-area-container">
						<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('blog-sidebar')) : else : ?>
							<p><strong>Widget Ready</strong></p>  
						<?php endif; ?>  
					</aside>
					<div class="error-sitemap-menu">
						<?php wp_nav_menu( array( 'theme_location' => 'error-menu' ) ); ?> 
					</div>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
