<?php
/**
 * Template Name: Contact
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package applied-handling
 */

get_header(); ?>

<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?> 


        <div class="slides">
            <ul>
                <li class="slide" style="background-image: url(<?php echo $src[0]; ?>);">
                    <div class="wrapper">
                        <h1 class="title">Contact Us</h1>
                        <div class="breadcrumbs">
                            <a href="/">Home</a>
                            <span class="separator">&gt;</span>
                            <span class="current">Contact Us</span>
                        </div>
                    </div>
                    <div class="header-overlay"></div>
                </li>
            </ul>
        </div>

        <div class="content contact-content">
            <div class="wrapper">
                <div class="form-wrapper">

                    <div class="form-header">
                        <h2>Say Hello!</h2>
                        <p>We would love to hear from you!</p>
                    </div>

                    <?php the_content(); ?>

                </div>

                <div class="contact-locations">

                    <div class="location">

                        <h3 class="title">Dearborn</h3>

                        <p class="address">
                            Applied Handling<br/>
                            15200 Century Dr.<br/>
                            Dearborn, MI 48120
                        </p>

                        <a class="button phone" href="tel:3133368020" onClick="ga('send', 'event', { eventCategory: 'Phone Number', eventAction: 'Click', eventLabel: '<?php the_title() ?> | 313-336-8020'});"><span class="icon"></span><span class="number">(313) 336-8020</span></a>
                        <a class="button service" href="#"><span class="icon"></span>Service Area</a>

                        <img class="service-area" src="/wp-content/themes/applied-handling/assets/images/dearborn-clio-service-area.png">

                    </div>

                    <div class="location">

                        <h3 class="title">Flint</h3>

                        <p class="address">
                            Applied Handling<br/>
                            221 S. Mill Street Ste 2<br/>
                            Clio, MI 48420
                        </p>

                        <a class="button phone" href="tel:8102759490" onClick="ga('send', 'event', { eventCategory: 'Phone Number', eventAction: 'Click', eventLabel: '<?php the_title() ?> | 810-275-9490'});"><span class="icon"></span><span class="number">(810) 275-9490</span></a>
                        <a class="button service" href="#"><span class="icon"></span>Service Area</a>

                        <img class="service-area" src="/wp-content/themes/applied-handling/assets/images/flint-service-area.png">

                    </div>

                    <div class="location">

                        <h3 class="title">Grand Rapids</h3>

                        <p class="address">
                            Applied Handling<br/>
                            7425 Clyde Park SW Ste. E<br/>
                            Byron Center, MI 49315
                        </p>

                        <a class="button phone" href="tel:6168785370" onClick="ga('send', 'event', { eventCategory: 'Phone Number', eventAction: 'Click', eventLabel: '<?php the_title() ?> | 616-878-5370'});"><span class="icon"></span><span class="number">(616) 878-5370</span></a>
                        <a class="button service" href="#"><span class="icon"></span>Service Area</a>

                        <img class="service-area" src="/wp-content/themes/applied-handling/assets/images/grand-rapids-service-area.png">

                    </div>

                </div>

            </div>
        </div>

        <div id="map">

            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2950.58347986106!2d-83.18847088427668!3d42.308752246332325!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x883b346008343ab3%3A0x6cb75a9b882fef70!2s15200+Century+Dr%2C+Dearborn%2C+MI+48120!5e0!3m2!1sen!2sus!4v1462477970513" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

        </div>

        
<?php
get_sidebar();
get_footer();
