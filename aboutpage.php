<?php
/**
 * Template Name: About
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package applied-handling
 */

get_header(); ?>

<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?> 



        <div class="slides">
            <ul>
                <li class="slide" style="background-image: url(<?php echo $src[0]; ?>);">
                    <div class="wrapper">
                        <h1 class="title">About Us</h1>
                        <div class="breadcrumbs">
                            <a href="#">Home</a>
                            <span class="separator">&gt;</span>
                            <span class="current">About</span>
                        </div>
                    </div>
                    <div class="header-overlay"></div>
                </li>
            </ul>
        </div>

        <div class="content about-content">
            <div class="wrapper">

                <div class="top">

                    <div id="about-intro-text" class="text">
                        <?php the_field( 'intro_text' ) ?>
                    </div>
                    <div style="clear: both"></div>
                </div>

                <div class="bottom" style="text-align: center;">

                    <img class="hex-building" style="display: inline-block; float: right;" src="/wp-content/themes/applied-handling/assets/images/hex-building.png" />

                    <div id="about-intro-text-two" class="text" style="text-align: right; padding-left:0px;">

                        <?php the_field( 'main_content' ) ?>

                    </div>

                    <div style="clear: both"></div>
                </div>

            </div>
        </div>

        <div class="quote-band">

            <p class="quote"><?php the_field( 'quote' ) ?></p>

            <p class="signature"><?php the_field( 'quotee' ) ?></p>

        </div>

        <h3 id="history">Our History</h3>
        <div id="history" class="timeline-feed max-width">
            <div id="timeline-first" class="timeline-event even hidden">
                <div id="timeline-image"><img width="150" height="150" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/timeline-first.png" class="attachment-thumbnail size-thumbnail wp-post-image"></div>
                <div style="clear: both"></div>
            </div>
            <?php
              query_posts( array( 'post_type' => 'timelines', 'types' => 'who-we-are', 'orderby' => 'date', 'order' => 'ASC' ) );
              if ( have_posts() ) : while ( have_posts() ) : the_post();
            ?>
                <div class="timeline-event <?php echo (++$j % 2 == 0) ? 'even' : 'odd'; ?>">
                    <div id="timeline-image">  
                            <?php the_post_thumbnail( 'thumbnail' ); ?>
                    </div>
                    <div id="timeline-content">
                        <h3><?php the_title(); ?></h3>
                        <div class="entry">
                            <?php the_content(); ?>
                        </div>
                        <div class="timeline-bg">
                            <img src="<?php the_field( 'background_image' ) ?>" />
                        </div>
                    </div>
                    <div style="clear: both"></div>
                </div>
            <?php endwhile; endif; wp_reset_query(); ?>
            <div id="timeline-last" class="timeline-event odd hidden">
                <div id="timeline-image"><img width="150" height="150" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/timeline-last.png" class="attachment-thumbnail size-thumbnail wp-post-image"></div>
                <div style="clear: both"></div>
            </div>
            <div style="clear: both"></div>
        </div>



        <div class="today">

            <div class="wrapper">

                <h3 class="mobile-only">Today</h3>

                <div class="cols">
                    <div class="col col-2">

                        <?php the_field( 'today_column_one' ) ?>
                        
                    </div>
                    <div class="col col-2">

                        <?php the_field( 'today_column_two' ) ?>

                    </div>
                </div>

                <div class="quote-box">

                    <p class="quote"><?php the_field( 'today_quote' ) ?></p>

                </div>
            </div>

        </div>
        
<?php
get_sidebar();
get_footer();
