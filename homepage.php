<?php
/**
 * Template Name: Homepage
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package applied-handling
 */

get_header(); ?>

        <div class="slider-container">
            <?php if(function_exists("avartanslider")) avartanslider("homepage_slider"); ?>
            <div class="slider-overlay"></div>
        </div>

        <div class="categories">
            <div class="wrapper">

                <ul>
                    <li class="loading-docks" style="background-image: url(<?php the_field( 'docks_hexagon_image' ) ?>);">
                        <a href="/loading-docks">Loading Docks <span></span></a>
                    </li>
                    <li class="doors" style="background-image: url(<?php the_field( 'doors_hexagon_image' ) ?>);">
                        <a href="/industrial-doors/">Doors <span></span></a>
                    </li>
                    <li class="fans" style="background-image: url(<?php the_field( 'fans_hexagon_image' ) ?>);">
                        <a href="/industrial-fans/">Fans <span></span></a>
                    </li>
                    <li class="lifts-storage" style="background-image: url(<?php the_field( 'lifts_hexagon_image' ) ?>);">
                        <a href="/lifts-storage/">Lifts &amp; Storage <span></span></a>
                    </li>
                    <li class="gates-walls" style="background-image: url(<?php the_field( 'gates_hexagon_image' ) ?>);">
                        <a href="/barriers-gates-and-walls/">Gates &amp; Walls <span></span></a>
                    </li>
                    <li class="service" style="background-image: url(<?php the_field( 'service_hexagon_image' ) ?>);">
                        <a href="/service-construction/">Service <span></span></a>
                    </li>
                </ul>

            </div>
        </div>

        <div class="content">
            <div class="wrapper">

                <div class="col-md-2">

                    <div class="block col who-we-are">

                        <?php the_field( 'who_we_are' ) ?>

                    </div>

                    <div class="block col our-history">

                        <?php the_field( 'our_history' ) ?>

                    </div>
                </div>

            </div>
        </div>

        <div class="projects">

            <h3 class="title">Projects</h3>

            <div class="projects-list">
		
            	<?php
            	$args = array( 'post_type' => 'project', 'posts_per_page' => 6 );
            	$loop = new WP_Query( $args );
            	$count = 1;
            	while ( $loop->have_posts() ) : $loop->the_post();
            		$thumb_id = get_post_thumbnail_id();
            		$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
            		$thumb_url = $thumb_url_array[0];
            	?>
            		<style>
            			.projects .projects-list .project:nth-of-type(<?php echo $count; ?>) {
            				background: transparent url("<?php echo $thumb_url; ?>") no-repeat scroll center center / cover ;
            			}
            		</style>
            		<div class="project">

                                <a href="<?php the_permalink(); ?>"><div class="hover">
                                    <div class="hover-contents">
                                        <h3 class="description"><?php the_title(); ?></h3>
                                        <a class="view" href="<?php the_permalink(); ?>">View This Project <span></span></a>
                                    </div>
                                </div></a>

                            </div>
            	<?php
            		$count++;
            	endwhile;
            	?>
                <div style="clear: both"></div>
            </div>

        </div>

        <div class="posts">

            <h3 class="title" style="padding-top:35px;">Latest Blog Posts</h3>

            <div class="posts-list">
<?php
$args = array( 'post_type' => 'post', 'numberposts' => 3, 'order'=> 'DESC', 'orderby' => 'date' );
$args = array( 'numberposts' => 3, 'order'=> 'DESC', 'orderby' => 'date' );
$postslist = get_posts( $args );
foreach ($postslist as $post) :  setup_postdata($post);
?> 
                <div class="post">

                    <div class="post-feed-image"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a></div>
                    <div class="content">
                        <div class="meta"><span class="date"><?php the_date(); ?></span> <!--|  <span class="tags"><a href="#" class="tag">Tips</a></span>--></div>

                        <h3 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

                        <p class="excerpt"><?php echo get_excerpt(); ?> <a href="<?php the_permalink(); ?>">read more</a></p>

                        <a class="read-more" href="<?php the_permalink(); ?>">read more</a>

                        <div class="social">
                            <ul>
                                <li class="facebook"><a target="_blank" href="http://www.facebook.com/sharer/sharer.php?s=100&amp;p[url]=<?php the_permalink(); ?>">Facebook</a></li>
                                <li class="twitter"><a target="_blank" href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>;text=<?php the_title(); ?>&amp;via=ahinow">twitter</a></li>
                                <li class="linkedin"><a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>">LinkedIn</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
<?php endforeach; ?>

            </div>

        </div>
        
<?php
get_sidebar();
get_footer();
