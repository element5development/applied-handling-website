<?php
/**
 * Template Name: Project Archive
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package applied-handling
 */

get_header(); ?>

<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?> 

    <div class="slides">
        <ul>
            <li class="slide"  style="background-image: url(<?php echo $src[0]; ?>);">
                <div class="wrapper">
                    <h2 class="title">Projects</h2>
                    <div class="breadcrumbs">
                        <a href="/">Home</a>
                        <span class="separator">&gt;</span>
                        <span class="current">Projects</span>
                    </div>
                </div>
                <div class="header-overlay"></div>
            </li>
        </ul>
    </div>

	<div style="clear:both;"></div>


	<div class="post-feed project-feed">

		<?php
	    	$args = array( 'post_type' => 'project', 'posts_per_page' => -1 );
	    	$loop = new WP_Query( $args );
	    	$count = 1;
	    	while ( $loop->have_posts() ) : $loop->the_post();
	    		$thumb_id = get_post_thumbnail_id();
	    		$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
	    		$thumb_url = $thumb_url_array[0];
    	?>

            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			    <div class="post-feed-image"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a></div>
			    <div class="content">
			        <h3 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			        <p class="excerpt"><?php the_field( 'description_project' ) ?></p>
			        <a class="view" href="<?php the_permalink(); ?>">View This Project <span></span></a>
			    </div>
			</article>

	    	<?php
	    		$count++;
	    	endwhile; ?>

	        <div style="clear: both"></div>

	</div>
        
<?php
get_sidebar();
get_footer();
