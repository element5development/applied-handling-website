<?php
/**
 * Template Name: Product Category
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package applied-handling
 */

get_header();
global $post; ?>

<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?> 

        <div class="slides">
            <ul>
                <a name="top"></a>
                <li class="slide" style="background-image: url(<?php echo $src[0]; ?>);">
                    <div class="wrapper">
                        <h1 class="title"><?php the_title(); ?></h1>
                        <div class="breadcrumbs">
                            <a href="/">Home</a>
                            <span class="separator">&gt;</span>
                            <?php if($post->post_parent) {
                            	$parent_link = get_permalink($post->post_parent); ?>
                            	<a href="<?php echo $parent_link; ?>"><?php echo get_the_title($post->post_parent); ?></a>
                            	<span class="separator">&gt;</span>
                            <?php } ?>
                            <span class="current"><?php the_title(); ?></span>
                        </div>
                    </div>
                    <div class="header-overlay"></div>
                </li>
            </ul>
        </div>

        <div class="content product-content">
            <div class="wrapper">

                <div class="top">

                    <img class="hex-grey" src="/wp-content/themes/applied-handling/assets/images/hex-grey.png" />

                    <div class="text">

                        <!--<h4>Dock Levelers are designed to transition from building to truck.</h4>

                        <p>Dock Levelers are available in mechanical, pneumatic and hydraulic operating systems with a variety of sizes, capacities and options. Rite-Hite® levelers bring added safety with features like “Smooth Transition Technology” for reducing back fatigue and the Safe-T-Lip barrier to keep lift trucks from running off an open dock.</p>-->

			<?php
			the_content();
			?>

                    </div>
                </div>

            </div>
        </div>

        <div class="products">

            <div class="wrapper">

                <div class="links">
			<?php
				$category = get_category_by_slug($post->post_name);
				$args = array(
					'categories' => $category->term_id
				);
				$tags = get_category_tags($args);
				foreach ($tags as $tag) {
					$content .= "<a class=\"button\" href=\"#\">$tag->tag_name</a>";
				}
			?>
                </div>

		<?php
			//print_r($category);
			query_posts( array (
				'cat' => ($category->term_id)?$category->term_id:99999,
				'post_type' => 'product',
				'posts_per_page' => -1,
				'orderby' => 'date',
				'order' => 'asc' ) );
		?>

                <div class="product-list">

		<?php
			while ( have_posts() ) : the_post();
		?>

                    <div class="product">

                        <div class="thumb">

                            <?php the_post_thumbnail(); ?>

                        </div>

                        <div class="content">

                            <h3 class="title"><?php the_title(); ?></h3>

                            <?php the_content(); ?>

                        </div>

                    </div>

		<?php
			endwhile;
		?>

                </div>
		<?php
			wp_reset_query();
		?>
                <p class="load-more"><a class="button" href="#top">Back to Top</a></p>

            </div>
        </div>

        <footer>
            <div class="red-band stay-informed">
                <div class="wrapper">
                    <h3 class="title">Need more information?</h3>
                    <p class="about-cta-content">Contact us to learn more.</p>
                    <?php echo do_shortcode('[gravityform id="7" title="false" description="false"]'); ?>
                    <div style="clear: both"></div>
                </div>
            </div>
        </footer>
        
<?php
get_sidebar();
get_footer();
