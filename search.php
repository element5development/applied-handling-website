<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package applied-handling
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		if ( have_posts() ) : ?>

			<div class="slides">
	            <ul>
	                <li class="slide about-us">
	                    <div class="wrapper">
	                        <h1 class="title"><?php printf( esc_html__( 'Search Results for: %s', 'applied-handling' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
	                    </div>
	                </li>
	            </ul>
	        </div>

	        <div class="post-feed">
				<aside id="" class="widget-area-container">
					<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('blog-sidebar')) : else : ?>
						<p><strong>Widget Ready</strong></p>  
					<?php endif; ?>  
				</aside>
				<?php /* Start the Loop */
					while ( have_posts() ) : the_post();

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'template-parts/content', get_post_format() );

					endwhile; 
				?>
				<div style="clear: both"></div>
			</div>


			<?php the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_sidebar();
get_footer();
